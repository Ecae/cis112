import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Frog here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Frog extends Actor
{
    /**
     * Act - do whatever the Frog wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if(Greenfoot.getRandomNumber(100) > 10){
            turn(Greenfoot.getRandomNumber(91)-30);
            move(Greenfoot.getRandomNumber(15)+10);
        }
        if(isAtEdge())
        {
            turnTowards(300, 200);
            move(100);
        }
       
        if ( isTouching(Worm.class) ) 
        {
            removeTouching(Worm.class);
        }
    }    
}
