import greenfoot.*;

/**
 * The fly objects created from this class are not very smart. They essentially
 * sit around waiting to be eaten by other animals, namely frogs in the case of
 * this scenario.
 * 
 * This class has errors that must be fixed before the scenario will work. Correct the
 * errors, and then replace the author name with your name and the version with the date
 * you completed the assignment.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Fly  extends Animal
{
    /**
     * Act - do whatever the Fly wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if (atWorldEdge()) {
        turn(17);
    }       
    move(1);
    if (Greenfoot.getRandomNumber(100) > 90) {
        turn(Greenfoot.getRandomNumber(90)-45);
    }
   
    }  
}

