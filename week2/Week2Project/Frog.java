import greenfoot.*;

/**
 * A Frog will move to a new location at random. If it sees a Fly in its new location,
 * it will eat it.
 * 
 * This class has errors that must be fixed before the scenario will work. Correct the
 * errors, and then replace the author name with your name and the version with the date
 * you completed the assignment
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

public class Frog extends Animal
{
    /**
     * Act - the frog has a 10% chance of moving to a new location. If the frog does
     * moves, it needs to see if there is a fly in its new location. If there is, then
     * the frog eats the fly. If the frog reaches an edge, then it will turn towards
     * the center of the world and move 100 steps. If the frog has eaten all the flies
     * and is all alone, then Greenfoot stops.
     */

    public void act() 
    {
        if(Greenfoot.getRandomNumber(100) > 10){
            turn(Greenfoot.getRandomNumber(91)-45);
            move(Greenfoot.getRandomNumber(15)+20);
        }
        if(atWorldEdge())
        {
            turnTowards(300, 200);
            move(100);
        }
        
        if(canSee(Fly.class))
        {
            eat(Fly.class);
        }
        
        if(allAlone())
            Greenfoot.stop();
        }
        
        
    }
