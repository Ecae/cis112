import greenfoot.*;

/**
 * Write a description of class FrogWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class FrogWorld extends World
{

    /**
     * Constructor for objects of class FrogWorld.
     * 
     */
    public FrogWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 

        prepare();
    }

    /**
     * Prepare the world for the start of the program. That is: create the initial
     * objects and add them to the world.
     */
    private void prepare()
    {
        Fly fly = new Fly();
        addObject(fly, 554, 50);
        Fly fly2 = new Fly();
        addObject(fly2, 538, 89);
        Fly fly3 = new Fly();
        addObject(fly3, 534, 118);
        Fly fly4 = new Fly();
        addObject(fly4, 499, 131);
        Fly fly5 = new Fly();
        addObject(fly5, 490, 120);
        Fly fly6 = new Fly();
        addObject(fly6, 460, 105);
        Fly fly7 = new Fly();
        addObject(fly7, 377, 64);
        Fly fly8 = new Fly();
        addObject(fly8, 368, 64);
        Fly fly9 = new Fly();
        addObject(fly9, 244, 200);
        Fly fly10 = new Fly();
        addObject(fly10, 79, 340);
        Fly fly11 = new Fly();
        addObject(fly11, 147, 361);
        Fly fly12 = new Fly();
        addObject(fly12, 246, 361);
        Fly fly13 = new Fly();
        addObject(fly13, 295, 302);
        Fly fly14 = new Fly();
        addObject(fly14, 349, 301);
        Fly fly15 = new Fly();
        addObject(fly15, 405, 342);
        Fly fly16 = new Fly();
        addObject(fly16, 441, 359);
        Fly fly17 = new Fly();
        addObject(fly17, 461, 357);
        Fly fly18 = new Fly();
        addObject(fly18, 529, 247);
        Fly fly19 = new Fly();
        addObject(fly19, 529, 143);
        Fly fly20 = new Fly();
        addObject(fly20, 463, 198);
        Fly fly21 = new Fly();
        addObject(fly21, 455, 191);
        Fly fly22 = new Fly();
        addObject(fly22, 445, 189);
        Fly fly23 = new Fly();
        addObject(fly23, 325, 107);
        Fly fly24 = new Fly();
        addObject(fly24, 260, 95);
        Fly fly25 = new Fly();
        addObject(fly25, 211, 89);
        Fly fly26 = new Fly();
        addObject(fly26, 179, 51);
        Fly fly27 = new Fly();
        addObject(fly27, 150, 47);
        Fly fly28 = new Fly();
        addObject(fly28, 108, 58);
        Fly fly29 = new Fly();
        addObject(fly29, 108, 82);
        Fly fly30 = new Fly();
        addObject(fly30, 108, 102);
        Fly fly31 = new Fly();
        addObject(fly31, 108, 102);
        Fly fly32 = new Fly();
        addObject(fly32, 86, 156);
        Fly fly33 = new Fly();
        addObject(fly33, 205, 168);
        Fly fly34 = new Fly();
        addObject(fly34, 206, 166);
        Fly fly35 = new Fly();
        addObject(fly35, 120, 205);
        Fly fly36 = new Fly();
        addObject(fly36, 122, 283);
        Fly fly37 = new Fly();
        addObject(fly37, 180, 283);
        Fly fly38 = new Fly();
        addObject(fly38, 322, 293);
        Fly fly39 = new Fly();
        addObject(fly39, 353, 209);
        Fly fly40 = new Fly();
        addObject(fly40, 381, 148);
        Fly fly41 = new Fly();
        addObject(fly41, 394, 217);
        Fly fly42 = new Fly();
        addObject(fly42, 461, 281);
        Fly fly43 = new Fly();
        addObject(fly43, 481, 297);
        Fly fly44 = new Fly();
        addObject(fly44, 487, 297);
        Fly fly45 = new Fly();
        addObject(fly45, 541, 345);
        Fly fly46 = new Fly();
        addObject(fly46, 545, 364);
        Fly fly47 = new Fly();
        addObject(fly47, 545, 329);
        Fly fly48 = new Fly();
        addObject(fly48, 572, 162);
        Fly fly49 = new Fly();
        addObject(fly49, 573, 212);
        Fly fly50 = new Fly();
        addObject(fly50, 556, 211);
        Fly fly51 = new Fly();
        addObject(fly51, 542, 207);
        Fly fly52 = new Fly();
        addObject(fly52, 476, 28);
        Fly fly53 = new Fly();
        addObject(fly53, 285, 53);
        Fly fly54 = new Fly();
        addObject(fly54, 309, 142);
        Fly fly55 = new Fly();
        addObject(fly55, 309, 142);
        Fly fly56 = new Fly();
        addObject(fly56, 287, 157);
        Fly fly57 = new Fly();
        addObject(fly57, 236, 139);
        Fly fly58 = new Fly();
        addObject(fly58, 179, 130);
        Fly fly59 = new Fly();
        addObject(fly59, 168, 188);
        Fly fly60 = new Fly();
        addObject(fly60, 185, 225);
        Fly fly61 = new Fly();
        addObject(fly61, 243, 243);
        Fly fly62 = new Fly();
        addObject(fly62, 332, 228);
        Fly fly63 = new Fly();
        addObject(fly63, 287, 211);
        Fly fly64 = new Fly();
        addObject(fly64, 329, 179);
        Fly fly65 = new Fly();
        addObject(fly65, 220, 292);
        Fly fly66 = new Fly();
        addObject(fly66, 357, 356);
        Fly fly67 = new Fly();
        addObject(fly67, 57, 65);
        Fly fly68 = new Fly();
        addObject(fly68, 54, 184);
        Fly fly69 = new Fly();
        addObject(fly69, 59, 281);
        Fly fly70 = new Fly();
        addObject(fly70, 64, 264);
        Fly fly71 = new Fly();
        addObject(fly71, 66, 233);
        Fly fly72 = new Fly();
        addObject(fly72, 152, 281);
        Fly fly73 = new Fly();
        addObject(fly73, 152, 281);
        Fly fly74 = new Fly();
        addObject(fly74, 302, 345);
        Fly fly75 = new Fly();
        addObject(fly75, 198, 362);
        Fly fly76 = new Fly();
        addObject(fly76, 561, 280);
        Fly fly77 = new Fly();
        addObject(fly77, 412, 259);
        Frog frog = new Frog();
        addObject(frog, 139, 155);
    }
}
