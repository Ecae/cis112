import greenfoot.*;

/**
 * Write a description of class Animal here.
 * 
 * @author (Meg Prescott) 
 * @version (2015-09-07)
 */
public abstract class Animal extends Actor
{
    /**
     * Returns true if the animal object is in the same
     * space as an object of a specified class and false
     * otherwise
     */
    public boolean canSee(java.lang.Class cls)
    {
        if(isTouching(cls))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Removes an object of a specified class if it
     * occupies the same space as the animal
     */
    public void eat(java.lang.Class cls)
    {
        removeTouching(cls);
    }

    /**
     * Returns true if the animal is at the edge of the 
     * world and false otherwise. Is exactly the same
     * as isAtEdge from the Actor class
     */
    public boolean atWorldEdge()
    {
        return isAtEdge();
    }
    
    /**
     * Returns true if the animal is the only object in 
     * the world, and false if there are other objects 
     * in the world along with the animal
     */
    public boolean allAlone()
    {
        World world = getWorld();
        if(world.numberOfObjects() == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
