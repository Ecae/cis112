import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

/**
 * This class defines a crab. Crabs live on the beach. They like sand worms 
 * (very yummy, especially the green ones).
 * 
 * Version: 5
 * 
 * In this version, the crab behaves as before, but we add animation of the 
 * image.
 */

public class Frog extends Actor
{
    private GreenfootImage image1;
    private GreenfootImage image2;
    public static int fliesEaten;
    
    /**
     * Create a crab and initialize its two images.
     */
    public Frog()
    {
        image1 = new GreenfootImage("frog.png");
        setImage(image1);
        fliesEaten = 0;
    }
        
    /** 
     * Act - do whatever the crab wants to do. This method is called whenever
     *  the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        checkKeypress();
        move(3);
        lookForFly();
    }
            
    /**
     * Check whether a control key on the keyboard has been pressed.
     * If it has, react accordingly.
     */
    public void checkKeypress()
    {
        if (Greenfoot.isKeyDown("left")) 
        {
            turn(-4);
        }
        if (Greenfoot.isKeyDown("right")) 
        {
            turn(4);
        }
    }
    
    /**
     * Check whether we have stumbled upon a worm.
     * If we have, eat it. If not, do nothing. If we have
     * eaten eight worms, we win.
     */
    public void lookForFly()
    {
        if ( isTouching(Fly.class) ) 
        {
            removeTouching(Fly.class);
            
            
           fliesEaten = fliesEaten + 1;
            
            if (fliesEaten == BugWorld.NUMBER_OF_Flies) 
            {
                
                Greenfoot.stop();
            }
        }
    }
}