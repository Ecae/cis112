import greenfoot.Greenfoot;
import greenfoot.GreenfootImage;
import greenfoot.World;
import java.util.List;

public class BattleBots extends World {
    private static final int WIDTH = 900;
    private static final int HEIGHT = 600;
    private static final int CELL_SIZE = 1;
    private static final int BUFFER = 100;
    private static String playAgain = "";
    private Robot frogBot;
    private Robot tuxBot;
    
    public static int tux;
    public static int frog;
   
    private Robot winner;
    private Robot loser;
    private ResultMessage resultMessage;
    private GreenfootImage frogWins;
    private GreenfootImage tuxWins;
    private GreenfootImage blank;
    private boolean running = true;
    private boolean gameOver = false;
    

    public BattleBots() {
        super(900, 600, 1);
        this.placeRobots();
        this.resultMessage = new ResultMessage();
        int x = this.getWidth() / 2;
        int y = this.getHeight() / 2;
        this.addObject(this.resultMessage, x, y);
        this.blank = new GreenfootImage("blankMessage.png");
        this.frogWins = new GreenfootImage("frogBotWin.png");
        this.tuxWins = new GreenfootImage("tuxBotWin.png");
     
        Scoreboard scoreboard = new Scoreboard();
        addObject(scoreboard,900,0);
        scoreboard.setLocation(800,30);
        
        FrogBoard frogboard = new FrogBoard();
        addObject(frogboard, 900, 0);
        frogboard.setLocation(500, 30);
       
    }

    public void placeRobots() {
        this.frogBot = new FrogBot();
        this.tuxBot = new TuxBot();
        
        int x = Greenfoot.getRandomNumber(250) + 100;
        int y = Greenfoot.getRandomNumber(400) + 100;
        this.addObject(this.frogBot, x, x);
        
        x = Greenfoot.getRandomNumber(250) + 450 + 100;
        y = Greenfoot.getRandomNumber(400) + 100;
        this.addObject(this.tuxBot, x, y);
        
    }

    public void act() {
        if(this.running && this.frogBot.foundOtherBot()) {
            Greenfoot.stop();
            this.running = false;
        }

    }

    public void stopped() {
        for(int winnerImage = 0; winnerImage < 10; ++winnerImage) {
            this.frogBot.fight();
            this.tuxBot.fight();
            
             
        }

        if(Greenfoot.getRandomNumber(100) < 50) {
            this.winner = this.frogBot;
            this.loser = this.tuxBot;
           
            this.resultMessage.setImage(this.frogWins);
            frog++;
        } else {
            this.winner = this.tuxBot;
            this.loser = this.frogBot;
            
            this.resultMessage.setImage(this.tuxWins);
            tux++;
        }

       this.removeObject(this.loser);
        this.winner.setRotation(0);
        this.winner.setLocation(75, 75);
        GreenfootImage var2 = this.winner.getImage();
        var2.scale(var2.getWidth() * 2, var2.getHeight() * 2);
        if(!this.gameOver) {
            playAgain = Greenfoot.ask("Would you like to play again? Y/N");
            if(playAgain.equals("y") || playAgain.equals("Y")) {
                removeObject(this.winner);
                placeRobots();
                running = true;
                Greenfoot.start();
            } 
            
            else {
                this.gameOver = true;
                frog = 0;
                tux = 0;
               
            }
        }

        this.resultMessage.setImage(this.blank);
    }

    public boolean isRunning() {
        return this.running;
    }
    
}