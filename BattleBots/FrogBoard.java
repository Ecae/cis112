import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.*;

/**
 *
 */
public class FrogBoard extends Actor
{
    private final int WIDTH = 115;
    private final int HEIGHT = 50;
    private final float FONT_SIZE = 14.0f;

    public FrogBoard()
    {
        
        updateImage();
    }
    
    public void act()
    {
        updateImage();
    }
   

    public void updateImage()
    {
        GreenfootImage image = new GreenfootImage(WIDTH, HEIGHT);
        image.setColor(new Color(0, 0, 0, 160));
        image.fillRect(0, 0, WIDTH, HEIGHT);
        image.setColor(new Color(255, 255, 255, 100));
        image.fillRect(5, 5, WIDTH-10, HEIGHT-10);
        Font font = image.getFont();
        font = font.deriveFont(FONT_SIZE);
        image.setFont(font);
        image.setColor(Color.WHITE);
        image.drawString("Frogbot wins: " + BattleBots.frog, 10, 16);
        setImage(image);

    }
}
